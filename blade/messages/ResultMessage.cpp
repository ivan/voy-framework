/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "ResultMessage.h"

#include <QDataStream>
#include <QDebug>

REGISTER_MESSAGE_TYPE(ResultMessage)

ResultMessage::ResultMessage(const ResultList &items)
    : Message(CREATE_MESSAGE_HEADER)
    , results(items)
{
}

void ResultMessage::serialize(QDataStream &out) const
{
    qDebug() << "<< " << results.size();
    out << (quint16)results.size();

    for (const auto &result: results) {
        qDebug() << "<< " << result;
        result.serialize(out);
    }
}


void ResultMessage::deserialize(QDataStream &in)
{
    quint16 size;
    in  >> size;

    results.resize(size);

    for (auto &result: results) {
        result.deserialize(in);
    }
}


QDebug &ResultMessage::debug(QDebug &out) const
{
    if (results.size() > 0) {
        return out << "ResultMessage(" << results[0] << "and " << (results.size() - 1) << "more)";
    } else {
        return out << "ResultMessage(EMPTY)";
    }
}


