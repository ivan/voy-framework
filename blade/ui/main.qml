import QtQuick 2.0
import org.kde.plasma.components 2.0

Rectangle {
    color: "black"

    anchors.fill: parent

    TextField {
        id: textQuery

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }

        onTextChanged: bladeModel.queryString = textQuery.text

        height: 32
    }


    ListView {
        anchors {
            top: textQuery.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        model: bladeModel
        // model: ListModel {
        //     id: fruitModel
        //
        //     ListElement {
        //         title: "Apple"
        //         cost: 2.45
        //     }
        // }

        delegate: Item {
            height: 100-32
            width: 500

            Rectangle {
                id: titleRect
                height: 32

                color: '#202020'
                anchors {
                    left: parent.left
                    right: parent.right
                    top: parent.top
                }

                Text {
                    text: model.title
                    anchors {
                        fill: parent
                        margins: 6
                    }
                    color: 'white'
                }
            }

            Rectangle {
                anchors {
                    left: parent.left
                    right: parent.right
                    top: titleRect.bottom
                }

                color: '#303030'
                height: 32

                Text {
                    text: model.id
                    anchors {
                        fill: parent
                        margins: 6
                    }
                    color: 'white'
                }

            }
        }

        add: Transition {
            NumberAnimation { properties: "x,y"; from: 100; duration: 1000 }
        }
        addDisplaced: Transition {
            NumberAnimation { properties: "x,y"; duration: 1000 }
        }
        displaced: Transition {
            NumberAnimation { properties: "x,y"; duration: 1000 }
        }
        move: Transition {
            NumberAnimation { properties: "x,y"; duration: 1000 }
        }
        moveDisplaced: Transition {
            NumberAnimation { properties: "x,y"; duration: 1000 }
        }
        // populate: Transition {
        //     NumberAnimation { properties: "x,y"; duration: 1000 }
        // }
        removeDisplaced: Transition {
            NumberAnimation { properties: "x,y"; duration: 1000 }
        }
        remove: Transition {
            ParallelAnimation {
                NumberAnimation { property: "opacity"; to: 0; duration: 1000 }
                NumberAnimation { properties: "x,y"; to: 100; duration: 1000 }
            }
        }
    }
}
