#include <QtGui>
#include <QtQuick>
#include <QDebug>
#include <QHash>
#include <QString>

#include <QApplication>
#include <QQuickView>

#include <KPluginLoader>
#include <KPluginMetaData>

#include <voy/Loop.h>
#include <voy/Protocol.h>

#include <memory>

#include "agents/Replicant.h"

#include "messages/QueryMessage.h"

#include "ResultModel.h"

#include "blade-features.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    const auto args = QCoreApplication::arguments();

    if (args.contains("--gui")) {

        if (args.contains("--all-runners")) {
            qDebug() << "Searching for plugins in" << BLADE_PLUGIN_DIR;
            const auto offers
                = KPluginLoader::findPlugins(QStringLiteral(BLADE_PLUGIN_DIR));

            qDebug() << "Count: " << offers.size();
            for (const auto& offer: offers) {
                qDebug() << "Offer:" << offer.pluginId();
                new Agents::Replicant(offer.pluginId());
            }
        }

        auto model = new ResultModel();

        qDebug() << "Starting";

        Voy::Loop::instance().start("", 0);

        QQuickView *view = new QQuickView();
        view->setSource(QUrl("qrc:/main.qml"));

        view->rootContext()->setContextProperty("bladeModel",     model);
        view->resize(300, 600);

        view->show();

    } else {
        bool somethingStarted = false;

        for (const auto& arg: args) {
            if (arg.startsWith("org.kde.plasma")) {
                new Agents::Replicant(arg);
                somethingStarted = true;
            }
        }

        if (!somethingStarted) return 1;
    }

    return app.exec();
}

