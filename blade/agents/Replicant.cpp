/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "Replicant.h"

#include <KPluginLoader>
#include <KPluginFactory>
#include <KPluginMetaData>

#include "blade-features.h"

#include <voy/MessageMatcher.h>

#include "messages/PingMessage.h"
#include "messages/PongMessage.h"
#include "messages/QueryMessage.h"
#include "messages/ResultMessage.h"

#include "Teams.h"

namespace Agents {

using std::swap;

Replicant::Replicant(const QString &engineId)
    : Voy::Agent(ReplicantTeam)
    , m_valid(false)
    , m_engineId(engineId)
{
    // 5 minutes should be enough for any runner to finish
    m_runnerTimeout.setInterval(5 * 60000);
    m_runnerTimeout.setSingleShot(true);
    QObject::connect(
        &m_runnerTimeout, &QTimer::timeout,
        &m_runnerTimeout, [this] {
            if (m_unassignedRunner) {
                Q_ASSERT_X(m_runners.empty(), "Replicant constructor",
                           "If we have an unassigned runner, the list of "
                           "assigned ones needs to be empty");
            } else {
                Q_ASSERT_X(!m_runners.empty(), "Replicant constructor",
                           "If we do not have an unassigned runner, we need to "
                           "have at least one assigned");

                swap(m_unassignedRunner, m_runners.begin()->second);

                m_runners.clear();
            }
        });

    m_unassignedRunner.reset(createRunnerInstace());

    if (m_unassignedRunner) {
        m_valid = true;
    }
}

Replicant::~Replicant()
{
}

void Replicant::message(Voy::MessagePtr message)
{
    if (!m_valid) return;

    dbg(this, Voy::Info)
        << "Replicant"
        << m_engineId
        << "got the message"
        << message;

    message
        | V_MATCH(PingMessage) {
            Q_UNUSED(_);
            reply<PongMessage>(m_engineId, m_engineName);
        }

        | V_MATCH(QueryMessage) {
            const auto from = _.from();

            if (!m_runners.count(from)) {
                if (m_runners.empty()) {
                    Q_ASSERT_X(m_unassignedRunner, "Replicant QueryMessage",
                               "We need an unassigned runner if the list"
                               "of the assigned ones are empty");

                    swap(m_unassignedRunner, m_runners[from]);

                } else {
                    m_runners[from].reset(createRunnerInstace());
                }
            }

            // This is cleaner, but ->message would work...
            // forward(m_runners[from]->address());
            if (!_.queryString.isEmpty()) {
                m_runners[from]->runQuery(_.queryString);
            } else {
                m_runners[from]->cancelQuery();
            }
        }
    ;
}

AbstractRunner *Replicant::createRunnerInstace() const
{
    auto offers = KPluginLoader::findPluginsById(QStringLiteral(BLADE_PLUGIN_DIR), m_engineId);

    if (offers.isEmpty()) {
        qWarning() << "[ FAILED ] not found: " << m_engineId;
        return nullptr;
    }

    auto plugin = offers.first();

    if (!plugin.isValid()) {
        qWarning() << "[ FAILED ] not valid: " << m_engineId;
        return nullptr;
    }

    KPluginLoader loader(plugin.fileName());
    KPluginFactory* factory = loader.factory();
    if (!factory) {
        qWarning() << "[ FAILED ] Could not load KPluginFactory for:"
                   << plugin.pluginId() << loader.errorString();
        return nullptr;
    }

    auto runner = factory->create<AbstractRunner>();

    if (!runner) {
        qWarning() << "[ FAILED ] instance creation failed for: "
                   << plugin.pluginId();
        return nullptr;
    }

    connect(runner, &AbstractRunner::startedProcessingQuery,
            this,   &Replicant::onStartedProcessingQuery);
    connect(runner, &AbstractRunner::newResultsAppeared,
            this,   &Replicant::onNewResults);
    connect(runner, &AbstractRunner::finishedProcessingQuery,
            this,   &Replicant::onFinishedProcessingQuery);

    return runner;
}

Voy::Address Replicant::senderRunnerAddress() const
{
    auto runner = sender();

    // TODO; Replace this with a multi-index map?
    for (const auto &item: m_runners) {
        if (runner == item.second.get()) {
            return item.first;
        }
    }

    return {};
}

void Replicant::onStartedProcessingQuery()
{

}

void Replicant::onNewResults(const ResultList &results)
{
    send<ResultMessage>(senderRunnerAddress(), results);
}

void Replicant::onFinishedProcessingQuery()
{

}

} // namespace Agents

