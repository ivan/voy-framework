/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "Collector.h"

#include <voy/Loop.h>

#include <QDebug>
#include <QCoreApplication>

#include <voy/Loop.h>
#include <voy/MessageMatcher.h>
#include <voy/utils/debug.h>
using Voy::Debug::pretty;

#include "messages/PingMessage.h"
#include "messages/PongMessage.h"
#include "messages/QueryMessage.h"
#include "messages/ResultMessage.h"

#include "Teams.h"

namespace Agents {

Collector::Collector()
    : Agent("Collectors")
    , m_nextQueryId(1)
{
}

void Collector::init()
{
    pingReplicants();

    // QTimer::singleShot(1000, [&] {
    //     send<QueryMessage>(Voy::localhost(ReplicantTeam), 1, "Query to search for");
    // });
}

void Collector::setQueryString(const QString &queryString)
{
    if (m_queryString == queryString) return;

    m_queryString = queryString;
    emit queryStringChanged(m_queryString);

    send<QueryMessage>(Voy::world(ReplicantTeam), m_nextQueryId++,
                       m_queryString);
}

QString Collector::queryString() const
{
    return m_queryString;
}

void Collector::message(Voy::MessagePtr message)
{
    message
        | V_MATCH(PongMessage) {
            dbg(this, Voy::Info) << "Got a PONG:" << _;
        }

        | V_MATCH(ResultMessage) {
            dbg(this, Voy::Info) << "Got results:" << _;
            emit newResultsAppeared(_.results);

        }
        // | V_OTHERWISE_IGNORE
    ;
}

void Collector::pingReplicants()
{
    dbg(this, Voy::Info) << "Pinging the world of runners";

    send<PingMessage>(Voy::localhost(ReplicantTeam));

    for (auto peerAddress: Voy::Loop::instance().availablePeers()) {
        dbg(this, Voy::Info) << "Channel disconnected.";
        peerAddress.teamName = ReplicantTeam;
        send<PingMessage>(peerAddress);
    }
}

bool Collector::isHostEnabled(const QUuid &hostId) const
{
    Q_UNUSED(hostId);
    // TODO: Add host pairing
    return true;
}

} // namespace Agents

