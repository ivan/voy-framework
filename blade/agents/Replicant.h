/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BLADE_AGENTS_REPLICANT_H
#define BLADE_AGENTS_REPLICANT_H

#include <voy/Agent.h>

#include <memory>
#include <unordered_map>
#include <map>

#include <QScopedPointer>
#include <QTimer>

#include "AbstractRunner.h"

namespace Agents {

class Replicant: public QObject, public Voy::Agent {
    Q_OBJECT

public:
    Replicant(const QString &engineId);
    ~Replicant();

    void message(Voy::MessagePtr message) override;

    AbstractRunner *createRunnerInstace() const;

private Q_SLOTS:
    void onStartedProcessingQuery();
    void onNewResults(const ResultList &result);
    void onFinishedProcessingQuery();

private:
    bool m_valid;
    QString m_engineId;
    QString m_engineName;

    Voy::Address senderRunnerAddress() const;


    std::unique_ptr<AbstractRunner> m_unassignedRunner;
    std::map<Voy::Address, std::unique_ptr<AbstractRunner>> m_runners;
    QTimer m_runnerTimeout;
};

} // namespace Agents

#endif // include guard end

