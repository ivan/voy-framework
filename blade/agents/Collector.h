/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BLADE_AGENTS_COLLECTOR_H
#define BLADE_AGENTS_COLLECTOR_H

#include <voy/Agent.h>

#include <Result.h>

#include <QTimer>

namespace Agents {

class Collector: public QObject, public Voy::Agent {
    Q_OBJECT
    Q_PROPERTY(QString queryString READ queryString WRITE setQueryString NOTIFY queryStringChanged)

public:
    Collector();

    void pingReplicants();

    void message(Voy::MessagePtr message) override;

    void init() override;

    QString queryString() const;

public Q_SLOTS:
    void setQueryString(const QString &queryString);

Q_SIGNALS:
    void queryStringChanged(const QString &queryString);

    void newResultsAppeared(const ResultList &results);

private:
    bool isHostEnabled(const QUuid &hostId) const;

    Voy::AddressList m_peers;
    QString m_queryString;
    quint32 m_nextQueryId;

};

} // namespace Agents

#endif // include guard end

