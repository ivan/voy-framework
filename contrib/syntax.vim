syn keyword voyMATCH V_MATCH V_OTHERWISE V_OTHERWISE_IGNORE
hi def link voyMATCH Debug

syn region voySEND start="send<" end=">"
syn region voyREPLY start="reply<" end=">"
syn keyword voyFORWARD forward

hi def link voySEND Keyword
hi def link voyREPLY Keyword
hi def link voyFORWARD Keyword

