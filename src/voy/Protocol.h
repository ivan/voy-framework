/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOY_PROTOCOL_H
#define VOY_PROTOCOL_H

namespace Voy {
namespace Protocol {


template <typename MessageType, typename Sender, typename ...Args>
void sendVia(Sender *sender, Args&&... args)
{
    sender->sendMessage(
        Voy::makeMessage<MessageType>(std::forward<Args>(args)...));
}

template <typename MessageType, typename ...Args>
void broadcast(Args&&... args)
{
    Loop::instance().sendMessage(
        Voy::makeMessage<MessageType>(std::forward<Args>(args)...));
}

inline void send(const Address &to, MessagePtr message)
{
    message->headerRef().to = to;
    Loop::instance().sendMessage(message);
}

template <typename MessageType, typename ...Args>
void send(const Address &to, Args&&... args)
{
    send(to, Voy::makeMessage<MessageType>(std::forward<Args>(args)...));
}

inline void send(const AddressList &toAddresses, MessagePtr message)
{
    for (const auto& to: toAddresses) {
        send(to, message);
    }
}

template <typename MessageType, typename ...Args>
void send(const AddressList &toAddresses, Args&&... args)
{
    send(toAddresses, Voy::makeMessage<MessageType>(std::forward<Args>(args)...));
}

} // namespace Protocol
} // namespace Voy


#endif // include guard end

