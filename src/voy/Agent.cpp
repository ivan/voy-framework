/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "Agent.h"
#include "Loop.h"
#include "Protocol.h"

#include <QMultiHash>
#include <QByteArray>
#include <QList>
#include <QSet>

#include <voy/utils/debug.h>

namespace Voy {

class Agent::Private {
public:
    Private()
        : agentId(QUuid::createUuid())
    {
    }

    QList<QByteArray> teams;
    MessagePtr currentMessage;

    QUuid agentId;
};

Agent::Agent(const QByteArray &team)
    : d(new Private())
{
    Loop::instance().registerLocalAgent(d->agentId, this);

    if (!team.isEmpty()) {
        addToTeam(team);
    }
}

Agent::~Agent()
{
    Loop::instance().unregisterLocalAgent(d->agentId);
    delete d;
}

void Agent::setTeam(const QByteArray &team)
{
    removeFromAllTeams();
    addToTeam(team);
}

void Agent::addToTeam(const QByteArray &team)
{
    Loop::instance().registerToLocalTeam(team, this);
    d->teams << team;
}

void Agent::removeFromTeam(const QByteArray &team)
{
    Loop::instance().unregisterFromLocalTeam(team, this);
    d->teams.removeAll(team);
}

void Agent::removeFromAllTeams()
{
    for (const auto &team: d->teams) {
        Loop::instance().unregisterFromLocalTeam(team, this);
    }
    d->teams.clear();
}

QList<QByteArray> Agent::teams() const
{
    return d->teams;
}

QUuid Agent::id() const
{
    return d->agentId;
}

Address Agent::address() const
{
    return agent(Loop::instance().hostId(), Loop::instance().groupId(), id());
}

void Agent::message(MessagePtr message)
{
    Q_UNUSED(message)
}

void Agent::processMessage(MessagePtr message)
{
    const auto &header = message->headerRef();

    // If we sent the message, but it was not meant
    // directly for us (aka talking to myself),
    // we do not want it
    if (header.from.agentId == id() &&
            header.to.agentId.isNull()
        ) {
        return;
    }

    d->currentMessage = message;
    this->message(message);
    d->currentMessage.reset();
}

void Agent::replyToCurrentMessage(MessagePtr message) const
{
    Q_ASSERT_X(d->currentMessage, "Agent::replyToCurrentMessage",
               "current message is null");
    // dbg(0, Info) << "Sending a reply message to: " << d->currentMessage->from();
    sendMessage(d->currentMessage->from(), message);
}

void Agent::forward(const Address &to) const
{
    Voy::Protocol::send(to, d->currentMessage);
}

void Agent::sendMessage(const Address &to, MessagePtr message) const
{
    message->headerRef().from.scope   = Address::Agent;
    message->headerRef().from.agentId = d->agentId;

    Voy::Protocol::send(to, message);
}

void Agent::init()
{
}

} // namespace Voy

