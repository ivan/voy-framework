/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOY_MESSAGE_TYPE_H
#define VOY_MESSAGE_TYPE_H

#include "voy_export.h"

#include <QHash>

#include <functional>

#include "Address.h"
#include "Message.h"

namespace Voy {

class Message;

namespace Categories {

    enum {
        Unknown             = 0,

        VoyInternal_Sync    = 1,
        VoyInternal_Network = 2,

        UI                  = 64,
        Test                = 65,

        UserCategory        = 128
    };

} // namespace Categories

class VOY_EXPORT MessageTypes {
private:
    typedef std::function<Message*()> MessageConstructor;

public:
    static MessageTypes &self();

    template <typename Message,
              quint16 Category = Message::message_category,
              quint16 Type     = Message::message_type>
    static void registerType()
    {
        registerType(Category, Type, [] { return new Message(); });
    }

    static void registerType(int category, int type, MessageConstructor constructor);
    static bool isTypeRegistered(int category, int type);
    static MessagePtr create(int category, int type);

private:
    MessageTypes() = default;

    QHash<QPair<int, int>, MessageConstructor> constructors;

};

} // namespace Voy

#define CREATE_MESSAGE_HEADER                                                  \
    Voy::MessageHeader(message_category, message_type)

#define CREATE_MESSAGE_HEADER_CUSTOM(CATEGORY, TYPE)                           \
    Voy::MessageHeader(CATEGORY, TYPE)

#define DEFINE_MESSAGE_TYPE(CATEGORY, TYPE)                                    \
    enum { message_category = CATEGORY, message_type = TYPE }

#define REGISTER_MESSAGE_TYPE(MessageType)                                     \
    namespace {                                                                \
    struct StaticConstructorRegistration {                                     \
        StaticConstructorRegistration()                                        \
        {                                                                      \
            Voy::MessageTypes::registerType<MessageType>();                    \
        }                                                                      \
    };                                                                         \
    static StaticConstructorRegistration instance;                             \
    }

#endif // include guard end

