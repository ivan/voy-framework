/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "Loop.h"
#include "Agent.h"

#include "network/ConnectionManager.h"

#include <QCoreApplication>
#include <QUuid>
#include <QTimer>

#include <KConfig>
#include <KConfigGroup>

#include "utils/debug.h"

using Voy::Debug::pretty;

namespace Voy {

class Loop::Private {
public:
    Private()
      : connectionManager(nullptr)
      , groupId(QUuid::createUuid())
    {
        KConfig config("voyexperimentalrc");
        KConfigGroup confgroup(&config, "HostInfo");

        hostId = QUuid(confgroup.readEntry("ID", QByteArray()));

        if (hostId.isNull()) {
            hostId = QUuid::createUuid();
            confgroup.writeEntry("ID", hostId.toByteArray());
            config.sync();
        }
    }

    ConnectionManager *connectionManager;
    AddressList availablePeers;

    QHash<QUuid, Agent*> agents;
    QMultiHash<QByteArray, Agent*> teams;

    QUuid hostId;
    QUuid groupId;
};

Loop &Loop::instance()
{
    static Loop s_instance;
    return s_instance;
}

Loop::Loop()
    : d(new Private())
{
    d->connectionManager = new ConnectionManager(this);

    connect(d->connectionManager, &ConnectionManager::messageReceived,
            this,                 &Loop::processMessageLocally);

    connect(d->connectionManager, &ConnectionManager::peerConnected,
            this, [this] (const Address &peerAddress) {
                if (!d->availablePeers.contains(peerAddress)) {
                    d->availablePeers << peerAddress;
                    emit peerConnected(peerAddress);
                }
            });

    connect(d->connectionManager, &ConnectionManager::peerDisconnected,
            this, [this] (const Address &peerAddress) {
                if (d->availablePeers.contains(peerAddress)) {
                    d->availablePeers.removeAll(peerAddress);
                    emit peerDisconnected(peerAddress);
                }
            });
}

void Loop::sendMessage(MessagePtr message)
{
    auto &header = message->headerRef();
    header.from.hostId  = d->hostId;
    header.from.groupId = d->groupId;

    // Sending the message to our own agents
    if (header.to.scope == Address::World
        || (header.to.scope == Address::Host && header.to.hostId == hostId())
        || (header.to.groupId == groupId())) {
        processMessageLocally(message);
    }

    // Sending the message to our peers
    d->connectionManager->sendMessage(message);
}

void Loop::processMessageLocally(MessagePtr message)
{
    const auto &to = message->to();

    // qDebug() << ">>>"
    //     << "\n - " << to.hostId << hostId()
    //     << "\n - " << to.groupId << groupId()
    //     ;

    // This guard exists for the messages that are coming from the
    // outer world.

    Q_ASSERT_X(
        to.scope == Address::World ||
        (to.scope == Address::Host && to.hostId == hostId()) ||
        (to.scope == Address::Group && to.hostId == hostId() && to.groupId == groupId()) ||
        (to.scope == Address::Agent && to.hostId == hostId() && to.groupId == groupId() && d->agents.contains(to.agentId)),
        "Loop::processMessageLocally", "This message should have not reached us");

    // TODO: This is kinda repeated in a few places... check it out
    if (to.scope == Address::World
        || (to.scope == Address::Host && to.hostId == hostId())
        || (to.scope == Address::Group && to.groupId == groupId())) {

        // We are targetting multiple agents, do
        // we have a team to notify?
        if (to.teamName.isEmpty()) {
            // Notifying all agents
            for (const auto& agent: d->agents) {
                agent->processMessage(message);
            }

        } else {
            // Notifying only a single team
            for (const auto& agent: d->teams.values(to.teamName)) {
                agent->processMessage(message);
            }
        }

    } else if (to.scope == Address::Agent) {

        // This targets a single agent
        if (d->agents.contains(to.agentId)) {
            d->agents[to.agentId]->processMessage(message);
        }
    }
}

QUuid Loop::hostId() const
{
    return d->hostId;
}

QUuid Loop::groupId() const
{
    return d->groupId;
}

AddressList Loop::availablePeers() const
{
    return d->availablePeers;
}

void Loop::registerLocalAgent(QUuid agentId, Agent *agent)
{
    if (d->agents.contains(agentId)) return;
    d->agents[agentId] = agent;
}

void Loop::unregisterLocalAgent(QUuid agentId)
{
    d->agents.remove(agentId);
}

void Loop::registerToLocalTeam(QByteArray team, Agent *agent)
{
    if (!d->teams.contains(team, agent)) {
        d->teams.insert(team, agent);
    }
}

void Loop::unregisterFromLocalTeam(QByteArray team, Agent *agent)
{
    d->teams.remove(team, agent);
}


void Loop::start(QByteArray applicationName, quint16 defaultPort)
{
    // Start this only when we get the event loop
    qDebug() << "Init";
    QTimer::singleShot(0, this, [this] {
            for (const auto &agent: d->agents) {
                agent->init();
            }
        });
}

} // namespace Voy


