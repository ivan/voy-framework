/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "Address.h"

#include <voy/utils/debug.h>

#include "Loop.h"

namespace Voy {

Address::Address()
    : scope(Invalid)
{
}

Address::Address(Scope scope,
                 QUuid hostId,
                 QUuid groupId,
                 QUuid agentId,
                 QByteArray teamName)
    : scope(scope)
    , hostId(hostId)
    , groupId(groupId)
    , agentId(agentId)
    , teamName(teamName)
{
}

Address agent(const QUuid &hostId, const QUuid &groupId, const QUuid &agentId)
{
    return Address(
            Address::Agent,
            hostId,
            groupId,
            agentId,
            QByteArray()
        );
}

Address group(const QUuid &hostId, const QUuid &groupId,
              const QByteArray &teamName)
{
    return Address(
            Address::Group,
            hostId,
            groupId,
            QUuid(),
            teamName
        );
}

Address group(const QByteArray &teamName)
{
    return group(Loop::instance().hostId(),
                 Loop::instance().groupId(),
                 teamName);
}

Address host(const QUuid &hostId, const QByteArray &teamName)
{
    return Address(
            Address::Host,
            hostId,
            QUuid(),
            QUuid(),
            teamName
        );
}

Address localhost(const QByteArray &teamName)
{
    return Address(
            Address::Host,
            Loop::instance().hostId(),
            QUuid(),
            QUuid(),
            teamName
        );
}

Address world(const QByteArray &teamName)
{
    return Address(
            Address::World,
            QUuid(),
            QUuid(),
            QUuid(),
            teamName
        );
}

bool Address::operator== (const Address &other) const
{
    return (scope == World && other.scope == World)
        || (scope == Host  && other.scope == Host
                           && hostId == other.hostId)
        || (scope == Group && other.scope == Group
                           && hostId == other.hostId
                           && groupId == other.groupId)
        || (scope == Agent && other.scope == Agent
                           && hostId == other.hostId
                           && groupId == other.groupId
                           && agentId == other.agentId)
        ;
}

bool Address::operator!= (const Address &other) const
{
    return !operator==(other);
}

QDataStream &operator<<(QDataStream &out, const Address &info)
{
    out << (quint8)info.scope;

    switch (info.scope) {
        case Address::Invalid:
            break;

        case Address::World:
            out << info.teamName;
            break;

        case Address::Host:
            out << info.hostId;
            out << info.teamName;
            break;

        case Address::Group:
            out << info.hostId;
            out << info.groupId;
            out << info.teamName;
            break;

        case Address::Agent:
            out << info.hostId;
            out << info.groupId;
            out << info.agentId;
            break;

        default:
            break;
    }

    return out;
}

QDataStream &operator>>(QDataStream &in, Address &info)
{
    quint8 _scope;
    in >> _scope;
    info.scope = (Address::Scope)_scope;

    switch (info.scope) {
        case Address::Invalid:
            break;

        case Address::World:
            in >> info.teamName;
            break;

        case Address::Host:
            in >> info.hostId;
            in >> info.teamName;
            break;

        case Address::Group:
            in >> info.hostId;
            in >> info.groupId;
            in >> info.teamName;
            break;

        case Address::Agent:
            in >> info.hostId;
            in >> info.groupId;
            in >> info.agentId;
            break;

        default:
            break;
    }

    return in;
}

QDebug operator<<(QDebug dbg, const Address &info)
{
    switch (info.scope) {
        case Address::Invalid:
            return dbg
                << DEBUG_COLOR_RED << "INVALID!" << COLOR_RESET;

        case Address::World:
            return dbg
                << DEBUG_COLOR_BLUE << "WORLD" << COLOR_RESET;

        case Address::Host:
            return dbg
                << DEBUG_COLOR_SLIM_PURPLE
                << "Host"
                << Debug::pretty(info.hostId)
                << (info.teamName.isEmpty() ? "" : info.teamName.constData())
                << COLOR_RESET;

        case Address::Group:
            return dbg
                << DEBUG_COLOR_SLIM_YELLOW
                << "Group"
                << Debug::pretty({ info.hostId, info.groupId })
                << (info.teamName.isEmpty() ? "" : info.teamName.constData())
                << COLOR_RESET;

        case Address::Agent:
            return dbg
                << DEBUG_COLOR_SLIM_CYAN
                << "Agent"
                << Debug::pretty({ info.hostId, info.groupId, info.agentId })
                << COLOR_RESET;

        default:
            return dbg;
    }
}

} // namespace Voy


