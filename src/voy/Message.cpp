/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "Message.h"

#include <QDataStream>
#include <QDebug>

#include "Loop.h"

#include <voy/utils/debug.h>

namespace Voy {

Message::Message(MessageHeader header)
    : m_header(header)
{
}


const MessageHeader &Message::headerRef() const
{
    return m_header;
}


MessageHeader &Message::headerRef()
{
    return m_header;
}


MessageHeader Message::header() const
{
    return m_header;
}


const Address &Message::from() const
{
    return m_header.from;
}


const Address &Message::to() const
{
    return m_header.to;
}


void Message::setHeader(const MessageHeader &header)
{
    m_header = header;
}

void Message::serialize(QDataStream &out) const
{
    Q_UNUSED(out)
}


void Message::deserialize(QDataStream &in)
{
    Q_UNUSED(in)
}


QDebug &Message::debug(QDebug &out) const
{
    out << m_header;
    return out;
}


QDebug operator<<(QDebug out, const MessagePtr &message)
{
    message->debug(out);
    return out;
}


QDebug operator<<(QDebug out, const Message &message)
{
    message.debug(out);
    return out;
}


} // namespace Voy

