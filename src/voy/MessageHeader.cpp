/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "MessageHeader.h"

#include <QDataStream>
#include <QDebug>

#include "Loop.h"

#include <voy/utils/debug.h>

namespace Voy {

MessageHeader::MessageHeader(quint16 messageCategory,
                             quint16 messageType,
                             Address to,
                             Status status)
    : messageCategory(messageCategory)
    , messageType(messageType)
    , status(status)
    , to(to)
{
    from.scope   = Address::Group;
    from.hostId  = Loop::instance().hostId();
    from.groupId = Loop::instance().groupId();
}


QDataStream &operator<< (QDataStream &output, const MessageHeader &header)
{
    return output
        << header.messageCategory
        << header.messageType
        << (quint16)header.status
        << header.from
        << header.to
        ;
}


QDataStream &operator>> (QDataStream &input, MessageHeader &header)
{
    quint16 newStatus = 0;

    input
        >> header.messageCategory
        >> header.messageType
        >> newStatus
        >> header.from
        >> header.to
        ;

    header.status = (MessageHeader::Status)newStatus;
    return input;
}


QDebug operator<< (QDebug dbg, const MessageHeader &header)
{
    return dbg
        << (
            header.status == MessageHeader::Ok         ? "Ok"    :
            header.status == MessageHeader::Error      ? "Error" :
            header.status == MessageHeader::Terminated ? "Term." :
                                                         "Unkn."
        )
        << COLOR_RESET
        << "From:" << header.from
        << "To:"   << header.to
        ;
}

} // namespace Voy

