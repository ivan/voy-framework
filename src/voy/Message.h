/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOY_MESSAGE_H
#define VOY_MESSAGE_H

#include "voy_export.h"

#include <QSharedPointer>

#include "MessageHeader.h"

namespace Voy {

class VOY_EXPORT Message {
public:
    Message(MessageHeader header = MessageHeader());
    virtual ~Message() = default;

    virtual void serialize(QDataStream &out) const;
    virtual void deserialize(QDataStream &in);
    virtual QDebug &debug(QDebug &out) const;

    template <typename MessageType,
              quint16 Category = MessageType::message_category,
              quint16 Type     = MessageType::message_type>
    bool is() const
    {
        return m_header.messageCategory == Category
            && m_header.messageType == Type;
    }

    template <typename MessageType>
    const MessageType &as() const
    {
        Q_ASSERT(is<MessageType>());
        return *static_cast<const MessageType*>(this);
    }

    const Address &from() const;
    const Address &to() const;

    MessageHeader &headerRef();
    const MessageHeader &headerRef() const;
    MessageHeader header() const;
    void setHeader(const MessageHeader &header);

private:
    MessageHeader m_header;

    friend QDebug operator<<(QDebug out, const Voy::Message &message);
};

typedef QSharedPointer<Message> MessagePtr;

template <typename MessageType, typename ...Args>
MessagePtr makeMessage(Args&&... args)
{
    return MessagePtr(new MessageType(std::forward<Args>(args)...));
}

VOY_EXPORT
QDebug operator<<(QDebug out, const Voy::Message &message);

VOY_EXPORT
QDebug operator<<(QDebug out, const Voy::MessagePtr &message);

} // namespace Voy

Q_DECLARE_METATYPE(Voy::Message)

#endif // include guard end

