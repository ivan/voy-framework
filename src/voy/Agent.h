/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOY_AGENT_H
#define VOY_AGENT_H

#include "Message.h"

class QByteArray;
class QUuid;

namespace Voy {

class VOY_EXPORT Agent {
public:
    Agent(const QByteArray &team = QByteArray());
    virtual ~Agent();

    void setTeam(const QByteArray &team);
    void addToTeam(const QByteArray &team);
    void removeFromTeam(const QByteArray &team);
    void removeFromAllTeams();

    QList<QByteArray> teams() const;

    QUuid id() const;
    Address address() const;

    virtual void init();
    virtual void message(MessagePtr message);

protected:
    template <typename MessageType, typename ...Args>
    void send(const Address &to, Args&&... args) const
    {
        sendMessage(to,
            Voy::makeMessage<MessageType>(std::forward<Args>(args)...));
    }

    template <typename MessageType, typename ...Args>
    void broadcast(Args&&... args) const
    {
        sendMessage(Voy::world(),
            Voy::makeMessage<MessageType>(std::forward<Args>(args)...));
    }

    template <typename MessageType, typename ...Args>
    void reply(Args&&... args) const
    {
        replyToCurrentMessage(
            Voy::makeMessage<MessageType>(std::forward<Args>(args)...));
    }

    void forward(const Address &to) const;

private:
    void replyToCurrentMessage(MessagePtr message) const;
    void sendMessage(const Address &to, MessagePtr message) const;
    void processMessage(MessagePtr message);

    class Private;
    Private* const d;

    friend class Loop;
};

} // namespace Voy

#endif // include guard end

