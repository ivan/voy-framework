/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOY_DEBUG_H
#define VOY_DEBUG_H

#include <QDebug>
#include <QVector>
#include <QCoreApplication>

#include "voy_export.h"

class QUuid;

namespace Voy {

enum DebugCategories {
    NetworkGeneral,
    NetworkSend,
    NetworkReceive,
    NetworkLow,

    Success,
    Error,
    Warning,
    Highlight,
    Info,
    Other
};

// Black       0;30     Dark Gray     1;30
// Blue        0;34     Light Blue    1;34
// Green       0;32     Light Green   1;32
// Cyan        0;36     Light Cyan    1;36
// Red         0;31     Light Red     1;31
// Purple      0;35     Light Purple  1;35
// Brown       0;33     Yellow        1;33
// Light Gray  0;37     White         1;37


#define COLOR_RESET "\033[0m"

#define DEBUG_COLOR_BLUE   "\033[34;1m"
#define DEBUG_COLOR_GREEN  "\033[32;1m"
#define DEBUG_COLOR_CYAN   "\033[36;1m"
#define DEBUG_COLOR_RED    "\033[31;1m"
#define DEBUG_COLOR_PURPLE "\033[35;1m"
#define DEBUG_COLOR_YELLOW "\033[33;1m"
#define DEBUG_COLOR_GRAY   "\033[30;1m"

#define DEBUG_COLOR_SLIM_BLUE   "\033[34m"
#define DEBUG_COLOR_SLIM_GREEN  "\033[32m"
#define DEBUG_COLOR_SLIM_CYAN   "\033[36m"
#define DEBUG_COLOR_SLIM_RED    "\033[31m"
#define DEBUG_COLOR_SLIM_PURPLE "\033[35m"
#define DEBUG_COLOR_SLIM_YELLOW "\033[33m"
#define DEBUG_COLOR_SLIM_GRAY   "\033[30m"

#define DEBUG_COLOR_VALUE  COLOR_RESET DEBUG_COLOR_SLIM_BLUE

namespace Debug {

VOY_EXPORT
extern bool ColoredDebug_lastWasNotNull;

VOY_EXPORT
void debugPrettyPointer(QDebug &dbg, void *ptr);

VOY_EXPORT
void debugPrettyUuid(QDebug &dbg, void *ptr);

struct VOY_EXPORT PrettyPointer {
    PrettyPointer(void *ptr);
    intptr_t ptr;
};

inline PrettyPointer pretty(void *ptr)
{
    return PrettyPointer(ptr);
}

inline PrettyPointer pretty(qint64 number)
{
    return PrettyPointer((void*)number);
}

VOY_EXPORT
QDebug &operator<<(QDebug &dbg, const PrettyPointer &val);

struct VOY_EXPORT PrettyUuid {
    PrettyUuid(const QUuid &uuid);
    PrettyUuid(QVector<QUuid> uuid);
    QVector<QUuid> uuids;
};

inline PrettyUuid pretty(const QUuid &uuid)
{
    return PrettyUuid(uuid);
}

inline PrettyUuid pretty(const QVector<QUuid> &uuids)
{
    return PrettyUuid(uuids);
}

VOY_EXPORT
QDebug &operator<<(QDebug &dbg, const PrettyUuid &val);


template <int Category>
class ColoredDebug {
public:
    ColoredDebug(const char *category, void *this_)
        : dbg(qDebug())
    {
        Q_UNUSED(category)

        if (ColoredDebug_lastWasNotNull != (!!this_)) {
            qDebug() << "\n";
        }
        ColoredDebug_lastWasNotNull = (!!this_);

        short ptrhash = (((intptr_t)this_) / 32) % 8;
        QByteArray basecol("\033[30m");
        basecol[3] = '0' + ptrhash;

        dbg << "[" << pretty(32 * qHash(QCoreApplication::applicationPid())) << "]";

        if (this_) {
            dbg << pretty(this_);
        }

        dbg << COLOR_RESET
            << (
                (Category == NetworkGeneral) ? DEBUG_COLOR_GRAY      :
                (Category == NetworkReceive) ? DEBUG_COLOR_CYAN      :
                (Category == NetworkSend)    ? DEBUG_COLOR_YELLOW    :
                (Category == NetworkLow)     ? DEBUG_COLOR_SLIM_GRAY :

                (Category == Warning)        ? DEBUG_COLOR_PURPLE    :
                (Category == Error)          ? DEBUG_COLOR_RED       :
                (Category == Success)        ? DEBUG_COLOR_GREEN     :
                (Category == Highlight)      ? DEBUG_COLOR_SLIM_CYAN :
                (Category == Info)           ? DEBUG_COLOR_SLIM_GRAY :
                COLOR_RESET
            );
    }

    ~ColoredDebug()
    {
        dbg << COLOR_RESET;
    }

    template <typename T>
    ColoredDebug operator<<(const T &value) {
        dbg << value;
        return *this;
    }

private:
    QDebug dbg;

};


template <int Category>
ColoredDebug<Category> makeColoredDebug(const char *category, void *this_)
{
    return ColoredDebug<Category>(category, this_);
}

} // namespace Debug

#define dbg(This, Category) Voy::Debug::makeColoredDebug<Category>(#Category, This)

} // namespace Voy

#endif // include guard end

