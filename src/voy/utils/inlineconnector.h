/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOY_INLINE_CONNECTOR_H
#define VOY_INLINE_CONNECTOR_H

// Inline syntax for connections

namespace Voy {

namespace InlineConnector {

template <typename Source, typename Signal> class Helper {
public:
    Helper(const Source *source, Signal signal, const QObject *receiver)
        : m_source(source), m_signal(signal), m_receiver(receiver)
    {
    }

    template <typename Handler>
    QMetaObject::Connection operator|(Handler &&handler)
    {
        return QObject::connect(m_source, m_signal, m_receiver,
                         std::forward<Handler>(handler));
    }

    const Source *m_source;
    Signal m_signal;
    const QObject *m_receiver;
};


template <typename Signal> class SignalReceiverContainer {
public:
    SignalReceiverContainer(Signal signal, const QObject *receiver)
        : m_signal(signal), m_receiver(receiver)
    {
    }

    Signal m_signal;
    const QObject *m_receiver;
};


template <typename Signal>
SignalReceiverContainer<Signal> make(Signal &&signal, QObject *receiver)
{
    return SignalReceiverContainer<Signal>(std::forward<Signal>(signal),
                                           receiver);
}

} // namespace InlineConnector


template <typename Source, typename Signal>
InlineConnector::Helper<Source, Signal>
operator|(const Source *sender,
          InlineConnector::SignalReceiverContainer<Signal> &&half)
{
    return InlineConnector::Helper<Source, Signal>(sender, half.m_signal,
                                                   half.m_receiver);
}


template <typename Source, typename Signal>
InlineConnector::Helper<Source, Signal>
operator|(const QExplicitlySharedDataPointer<Source> &sender,
          InlineConnector::SignalReceiverContainer<Signal> &&half)
{
    return InlineConnector::Helper<Source, Signal>(sender.data(), half.m_signal,
                                                   half.m_receiver);
}


template <typename Source, typename Signal,
          typename
          = typename std::enable_if<!std::is_pointer<Source>::value>::type>
InlineConnector::Helper<Source, Signal>
operator|(const Source &sender,
          InlineConnector::SignalReceiverContainer<Signal> &&half)
{
    return InlineConnector::Helper<Source, Signal>(&sender, half.m_signal,
                                                   half.m_receiver);
}

#ifdef VOY_TCP_CHANNEL_CONNECTION_SYNTAX

#define on_connected InlineConnector::make(&TcpChannel::connected, this) | [=]

#define on_disconnected                                                        \
    InlineConnector::make(&TcpChannel::disconnected, this) | [=]

#define on_server_list_received(VAR)                                           \
    InlineConnector::make(&TcpChannel::serverListReceived, this)               \
        | [=](PeerInfoList VAR)

#define on_new_message(VAR)                                                    \
    InlineConnector::make(&TcpChannel::messageReceived, this)                  \
        | [=](MessagePtr VAR)

#define on_error(VAR)                                                          \
    InlineConnector::make(&TcpChannel::error, this)                            \
        | [=](QAbstractSocket::SocketError VAR)

#endif


#ifdef VOY_QT_SOCKET_CONNECTION_SYNTAX

#define on_new_client_available                                                \
    InlineConnector::make(&QTcpServer::newConnection, this) | [=]

#define on_ready_read                                                          \
    InlineConnector::make(&QAbstractSocket::readyRead, this) | [=]

#endif


#ifdef VOY_KDNSSD_CONNECTION_SYNTAX

#define on_dnssd_service_resolved(VAR)                                         \
    InlineConnector::make(&KDNSSD::RemoteService::resolved, this)              \
        | [=](bool VAR)

#define on_dnssd_service_published(VAR)                                        \
    InlineConnector::make(&KDNSSD::PublicService::published, this)             \
        | [=](bool VAR)

#define on_dnssd_service_appeared(VAR)                                         \
    InlineConnector::make(&KDNSSD::ServiceBrowser::serviceAdded, this)         \
        | [=](KDNSSD::RemoteService::Ptr VAR)

#endif

} // namespace Voy

#endif // include guard end

