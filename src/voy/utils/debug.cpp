/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "debug.h"

#include <QUuid>

namespace Voy {
namespace Debug {

bool ColoredDebug_lastWasNotNull = false;

template <typename T>
inline QByteArray colorForNumber(T number)
{
    short ptrhash = std::abs(number / 32) % 8;
    QByteArray basecol("\033[30m");
    basecol[3] = '0' + ptrhash;

    return basecol;
}

void debugPretty(QDebug &dbg, intptr_t ptr)
{
    dbg.nospace()
        << COLOR_RESET
        << colorForNumber((intptr_t)ptr).constData()
        << QByteArray::number((qlonglong)(intptr_t)ptr, 36).constData()
        << COLOR_RESET;
}

void debugPretty(QDebug &dbg, const QVector<QUuid> &uuids)
{
    dbg.nospace()
        << COLOR_RESET
        << "{ ";

    for (const auto &uuid: uuids) {
        if (uuid.isNull()) {
            dbg << "------ ";
        } else {
            auto bytes = uuid.toRfc4122().mid(0, 4);
            dbg.nospace()
                << COLOR_RESET
                << colorForNumber(*bytes.rbegin()).constData()
                << bytes.toBase64(QByteArray::OmitTrailingEquals).constData()
                << ' ';
        }
    }

    dbg.nospace()
        << COLOR_RESET
        << "}";
}

PrettyPointer::PrettyPointer(void *ptr)
    : ptr((intptr_t)ptr)
{
}

PrettyUuid::PrettyUuid(const QUuid &uuid)
    : uuids({uuid})
{
}

PrettyUuid::PrettyUuid(QVector<QUuid> uuids)
    : uuids(uuids)
{
}

QDebug &operator<<(QDebug &dbg, const PrettyPointer &val)
{
    debugPretty(dbg, val.ptr);
    return dbg.space();
}

QDebug &operator<<(QDebug &dbg, const PrettyUuid &val)
{
    debugPretty(dbg, val.uuids);
    return dbg.space();
}


} // namespace Debug
} // namespace Voy


