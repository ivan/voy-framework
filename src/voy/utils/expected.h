/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOY_EXPECTED_H
#define VOY_EXPECTED_H

namespace Voy {

namespace detail {
template <typename Value>
class expected_value {
public:
    expected_value(Value value)
        : m_value(value)
    {
    }

    const Value &value() const
    {
        return m_value;
    }

private:
    Value m_value;
};

template <typename Error>
class expected_error {
public:
    expected_error(Error error)
        : m_error(error)
    {
    }

    const Error &error() const
    {
        return m_error;
    }

private:
    Error m_error;
};

} // namespace detail

template <typename Value, typename Error>
class expected {
public:
    expected(detail::expected_value<Value> other)
        : m_valid(true)
        , m_value(other.value())
    {
    }

    expected(detail::expected_error<Error> other)
        : m_valid(false)
        , m_error(other.error())
    {
    }

    // Normal value constructor
    expected(const Value &value)
        : m_valid(true)
        , m_value(value)
    {
    }

    // Moving the value inside expected
    expected(Value &&value)
        : m_valid(true)
        , m_value(std::move(value))
    {
    }

    // Copy constructor
    expected(const expected &other)
        : m_valid(other.m_valid)
    {
        // We need to perform the in-place construction
        if (m_valid) {
            new (&m_value) Value(other.m_value);
        } else {
            new (&m_error) Error(other.m_error);
        }
    }

    // Move constructor
    expected(expected &&other)
        : m_valid(std::move(other.m_valid))
    {
        // We need to perform the in-place construction
        if (m_valid) {
            new (&m_value) Value(std::move(other.m_value));
        } else {
            new (&m_error) Error(std::move(other.m_error));
        }
    }

    // Assign
    expected operator= (expected other) = delete;

    ~expected()
    {
        if (m_valid) {
            m_value.~Value();
        } else {
            m_error.~Error();
        }
    }

    bool valid() const
    {
        return m_valid;
    }

    const Value &value() const
    {
        Q_ASSERT(m_valid);
        return m_value;
    }

    const Error &error() const
    {
        Q_ASSERT(!m_valid);
        return m_error;
    }

    template <typename ErrorSource>
    static expected from_error(ErrorSource &&error)
    {
        expected result;
        result.m_valid = false;
        new (&result.m_error) Error(std::forward<Error>(error));
        return result;
    }

private:
    bool m_valid;
    union {
        Value m_value;
        Error m_error;
    };

};


template <typename Value>
detail::expected_value<typename std::decay<Value>::type> make_expected(Value &&value)
{
    return detail::expected_value<typename std::decay<Value>::type>(std::forward<Value>(value));
}

template <typename Error>
detail::expected_error<typename std::decay<Error>::type> make_unexpected(Error &&error)
{
    return detail::expected_error<typename std::decay<Error>::type>(std::forward<Error>(error));
}

} // namespace Voy

#endif // include guard end

