/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOY_LOOP_H
#define VOY_LOOP_H

#include <QObject>

#include "Message.h"

#include "voy_export.h"

namespace Voy {

class Agent;

class VOY_EXPORT Loop: public QObject {
    Q_OBJECT

public:
    static Loop &instance();

    QUuid hostId() const;
    QUuid groupId() const;

    AddressList availablePeers() const;

    void start(QByteArray applicationName = QByteArray(),
               quint16 defaultPort = 0);

    void registerLocalAgent(QUuid agentId, Agent *agent);
    void unregisterLocalAgent(QUuid agentId);
    void registerToLocalTeam(QByteArray team, Agent *agent);
    void unregisterFromLocalTeam(QByteArray team, Agent *agent);

Q_SIGNALS:
    void peerConnected(const Address &peerAddress);
    void peerDisconnected(const Address &peerAddress);

public Q_SLOTS:
    void sendMessage(MessagePtr message);
    void processMessageLocally(MessagePtr message);

private:
    Loop();

    class Private;
    Private* const d;
};

} // namespace Voy

#endif // include guard end

