/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOY_MESSAGE_HEADER_H
#define VOY_MESSAGE_HEADER_H

#include "voy_export.h"

#include <QObject>

#include "Address.h"

namespace Voy {

class VOY_EXPORT MessageHeader {
    Q_GADGET

public:
    enum Status {
        Unknown    = 0,
        Ok         = 1,
        Error      = 2,
        Terminated = 3
    };
    Q_ENUM(Status)

    MessageHeader(quint16 messageCategory = 0,
                  quint16 messageType = 0,
                  Address to = world(),
                  Status status = Ok
                  );

    quint8 messageCategory;
    quint8 messageType;
    Status status;

    Address from;
    Address to;

    friend QDataStream &operator<<(QDataStream &out,
                                   const MessageHeader &header);
    friend QDataStream &operator>>(QDataStream &in, MessageHeader &header);

    friend QDebug operator<<(QDebug dbg, const MessageHeader &message);
};

VOY_EXPORT
QDataStream &operator<<(QDataStream &out,
                                   const MessageHeader &header);

VOY_EXPORT
QDataStream &operator>>(QDataStream &in, MessageHeader &header);

VOY_EXPORT
QDebug operator<<(QDebug dbg, const MessageHeader &message);

} // namespace Voy

#endif // include guard end

