/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "MessageTypes.h"

#include <QDataStream>
#include <QDebug>

#include "Loop.h"

#include <voy/utils/debug.h>

namespace Voy {

MessageTypes &MessageTypes::self()
{
    static MessageTypes self;
    return self;
}


void MessageTypes::registerType(int category, int type,
                                MessageConstructor constructor)
{
    self().constructors[qMakePair(category, type)] = constructor;
}


bool MessageTypes::isTypeRegistered(int category, int type)
{
    return self().constructors.contains(qMakePair(category, type));
}


MessagePtr MessageTypes::create(int category, int type)
{
    return
        isTypeRegistered(category, type)
        ? MessagePtr(self().constructors[qMakePair(category, type)]())
        : MessagePtr();
}

} // namespace Voy

