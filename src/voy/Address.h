/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOY_ADDRESS_H
#define VOY_ADDRESS_H

#include <QList>
#include <QUuid>
#include <QDebug>
#include <QDataStream>

#include "voy_export.h"

namespace Voy {

class VOY_EXPORT Address {
public:
    enum Scope {
        Invalid, // This is not a valid specification for the scope
        Agent,   // A single agent instance. Value `teamName` is ignored.
        Group,   // A process containing multiple agents. Value `agent` is ignored
        Host,    // All groups on a single host. Values `agent` and `group` are ignored
        World    // Everyone. All values but `teamName` are ignored.
    };

    Address(Scope scope,
            QUuid hostId,
            QUuid groupId,
            QUuid agentId,
            QByteArray teamName);

    Address();

    bool operator== (const Address &other) const;
    bool operator!= (const Address &other) const;

    Scope scope;

    QUuid hostId;
    QUuid groupId;
    QUuid agentId;

    QByteArray teamName;

    inline bool operator< (const Address &other) const
    {
        return std::tie(hostId, groupId, agentId) <
               std::tie(other.hostId, other.groupId, other.agentId);
    }

    friend QDataStream &operator<<(QDataStream &out, const Address &info);
    friend QDataStream &operator>>(QDataStream &in, Address &info);

    friend QDebug operator<<(QDebug dbg, const Address &info);
};

typedef QList<Address> AddressList;

VOY_EXPORT
Address agent(const QUuid &hostId, const QUuid &groupId, const QUuid &agentId);

VOY_EXPORT
Address group(const QUuid &hostId, const QUuid &groupId,
              const QByteArray &teamName = QByteArray());

VOY_EXPORT
Address host(const QUuid &hostId, const QByteArray &teamName = QByteArray());

VOY_EXPORT
Address group(const QByteArray &teamName = QByteArray());

VOY_EXPORT
Address localhost(const QByteArray &teamName = QByteArray());

VOY_EXPORT
Address world(const QByteArray &teamName = QByteArray());

VOY_EXPORT
QDataStream &operator<<(QDataStream &out, const Address &info);

VOY_EXPORT
QDataStream &operator>>(QDataStream &in, Address &info);

VOY_EXPORT
QDebug operator<<(QDebug dbg, const Address &info);

} // namespace Voy

Q_DECLARE_METATYPE(Voy::Address);
Q_DECLARE_METATYPE(Voy::AddressList);

#endif // include guard end

