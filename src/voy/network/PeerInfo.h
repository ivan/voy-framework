/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOY_NETWORK_PEER_INFO_H
#define VOY_NETWORK_PEER_INFO_H

#include <QUuid>
#include <QHostAddress>
#include <QDebug>
#include <QList>

class QDataStream;

namespace Voy {

class PeerInfo {
public:
    PeerInfo(QHostAddress host = QHostAddress(), quint16 port = 0,
             QUuid hostId = QUuid(), QUuid groupId = QUuid());

    PeerInfo(QUuid hostId, QUuid groupId);

    QHostAddress hostAddress;
    quint16 hostPort;

    QUuid hostId;
    QUuid groupId;

    bool operator== (const PeerInfo &other) const;

    bool operator!= (const PeerInfo &other) const;

    friend QDebug operator<<(QDebug out, const Voy::PeerInfo &peerInfo);

    friend QDataStream &operator<<(QDataStream &output,
                                   const Voy::PeerInfo &info);
    friend QDataStream &operator>>(QDataStream &input, Voy::PeerInfo &info);
};

typedef QList<PeerInfo> PeerInfoList;

} // namespace Voy

uint qHash(const Voy::PeerInfo &peerInfo);

Q_DECLARE_METATYPE(Voy::PeerInfo);
Q_DECLARE_METATYPE(Voy::PeerInfoList);

#endif // include guard end

