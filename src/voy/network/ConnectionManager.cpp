/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "ConnectionManager.h"
#include "ConnectionManager_p.h"

#include <QCoreApplication>
#include <QNetworkInterface>

#include <DNSSD/PublicService>
#include <DNSSD/RemoteService>

#include "../utils/debug.h"

#include "../Message.h"
#include "../Loop.h"
#include "common.h"
#include "TcpChannel.h"
#include "messages/HandshakeMessage.h"


#define VOY_TCP_CHANNEL_CONNECTION_SYNTAX
#define VOY_QT_SOCKET_CONNECTION_SYNTAX
#define VOY_KDNSSD_CONNECTION_SYNTAX
#include "../utils/inlineconnector.h"

namespace Voy {

ConnectionManager::Private::Private(ConnectionManager *parent)
    : QObject(parent)
    , q(parent)
    , dnssdBrowser("_" + VOY_APPLICATION_SLUG + "._tcp")
{
    printServiceStatusTimer.setSingleShot(true);
    printServiceStatusTimer.setInterval(1000);

    connect(&printServiceStatusTimer, &QTimer::timeout,
            this, &Private::printServiceStatus);

    // When we receive a new datagram, it means that we have a new
    // source to connect to
    udpListener | on_ready_read {
        while (udpListener.hasPendingDatagrams()) {
            PeerInfo peerInfo;
            QByteArray datagram;

            datagram.resize(udpListener.pendingDatagramSize());

            if (udpListener.readDatagram(datagram.data(), datagram.size(),
                                         &peerInfo.hostAddress, &peerInfo.hostPort) == -1) {
                continue;
            }

            if (!datagram.endsWith(":VoyService")) {
                continue;
            }

            QList<QByteArray> args = datagram.split(':');
            if (args.size() != 2) {
                continue;
            }

            peerInfo.hostPort = args.at(0).toUInt();

            dbg(0, NetworkLow) << "There is a new kid on the block:" << datagram;

            connectToSource(peerInfo);
        }
    };

    udpListener.bind(QHostAddress::Any, VOY_DEFAULT_PORT,
                     QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint);
}


ConnectionManager::~ConnectionManager()
{
    delete d;
}


void ConnectionManager::sendMessage(MessagePtr message)
{
    const auto &to = message->to();

    for (auto channel: d->validChannels) {
        const auto &peer = channel->peerInfo();

        if (to.scope == Address::World
            || (to.scope == Address::Host && to.hostId == peer.hostId)
            || (to.groupId == peer.groupId)) {

            channel->sendMessage(message);
        }
    }
}

void ConnectionManager::Private::connectToChannel(TcpChannel *channel)
{
    connectingChannels << channel;

    channel | on_disconnected {
        dbg(channel, Warning) << "Channel disconnected.";
        removeChannel(channel);
        printServiceStatusTimer.start();
    };

    channel | on_error(error) {
        dbg(channel, Error) << "Channel error:" << error;
        removeChannel(channel);
        printServiceStatusTimer.start();
    };

    channel | on_connected {
        // The channel is now a proper one, we have its ID and all

        const auto peerInfo    = channel->peerInfo();
        const auto peerGroupId = peerInfo.groupId;

        if (validChannels.contains(peerGroupId) &&
                validChannels[peerGroupId] != nullptr) {
            // If we already have a channel with the same id,
            // this one needs to be destroyed
            removeChannel(channel);

        } else {
            connectingChannels.remove(channel);
            validChannels[peerGroupId] = channel;
            peerInfos << peerInfo;

            connect(channel, &TcpChannel::messageReceived,
                    q,       &ConnectionManager::messageReceived);

            dbg(channel, Success) << "Got a new channel:"
                << DEBUG_COLOR_VALUE
                << peerInfo
                ;

            Q_ASSERT_X(!peerInfo.hostId.isNull(),
                       "ConnectionManager::connectToChannel | on_connected",
                       "The host ID can not be null");
            Q_ASSERT_X(!peerInfo.groupId.isNull(),
                       "ConnectionManager::connectToChannel | on_connected",
                       "The group ID can not be null");

            printServiceStatusTimer.start();

            emit q->peerConnected(channel->peerAddress());
        }
    };

    channel | on_server_list_received(serverList) {
        for (const auto &server: serverList) {
            connectToSource(server);
        }
    };

    printServiceStatusTimer.start();
}


void ConnectionManager::Private::removeChannel(TcpChannel *channel)
{
    const auto peerGroupId = channel->peerInfo().groupId;

    if (connectingChannels.contains(channel)
            || (validChannels.value(peerGroupId) != channel)) {
        // We are asked to remove a channel that we have not
        // been properly connected to yet, or a new channel for
        // something that we are already connected to,
        // which we do not need.
        connectingChannels.remove(channel);

    } else {
        // We are removing a channel we are already connected to
        validChannels.remove(peerGroupId);
        peerInfos.removeAll(
            PeerInfo(QUuid(), peerGroupId));

        emit q->peerDisconnected(channel->peerAddress());
    }

    channel->deleteLater();
}


bool ConnectionManager::Private::isLocalHost(const PeerInfo &peer) const
{
    if (peer.hostAddress == QHostAddress::LocalHost) return true;

    // Qt thinks these are different "::ffff:10.0.0.2" and "10.0.0.2"
    const auto peerIP = peer.hostAddress;
    const auto peerIPv4 = peer.hostAddress.toIPv4Address();
    // const auto peerIPv6 = peer.host.toIPv6Address();

    foreach (QNetworkInterface interface, QNetworkInterface::allInterfaces()) {
        foreach (QNetworkAddressEntry entry, interface.addressEntries()) {
            const auto selfIP = entry.ip();
            const auto selfIPv4 = selfIP.toIPv4Address();
            // const auto selfIPv6 = selfIP.toIPv6Address();

            if (selfIP == peerIP) return true;
            if (selfIPv4 && selfIPv4 == peerIPv4) return true;

            // if (std::equal(
            //             (char*)&selfIPv6, (char*)&selfIPv6 + 16,
            //             (char*)peerIPv4)) return true;
        }
    }

    return false;
}


void ConnectionManager::Private::connectToSource(const PeerInfo &peerInfo_)
{
    PeerInfo peerInfo = peerInfo_;

    if (isLocalHost(peerInfo)) {
        dbg(0, NetworkLow)
            << "    This is a local connection";
        peerInfo.hostAddress = QHostAddress::LocalHost;

        if (selfInfo.hostPort == peerInfo.hostPort) {
            dbg(0, NetworkLow)
                << "    I've received myself as the source. Ignoring.";

            return;
        }
    }

    dbg(0, NetworkGeneral)
        << "Initializing a new source:" << peerInfo;

    // selfInfo might not be initialized at this point,
    // but if it is, and the new service is actually us,
    // bail out
    if (peerInfo == selfInfo) {
        dbg(0, NetworkLow)
            << "    I've received myself as the source again. Ignoring.";
        return;
    }

    // If we already have this service, do not register it again
    if (peerInfos.contains(peerInfo)) {
        dbg(0, NetworkLow)
            << "    We already have this source registered. Ignoring."
            << peerInfos;
        return;
    }

    connectToChannel(new TcpChannel(peerInfo, *q));
}


void ConnectionManager::Private::initializeServer()
{
    // Trying the default port first, then any port
    if (!self.listen(QHostAddress::Any, VOY_DEFAULT_PORT)) {
        self.listen(QHostAddress::Any, 0);

        if (!self.isListening()) {
            qFatal("Can not create a TCP listener service");
        }
    }

    selfInfo.hostId  = Loop::instance().hostId();
    selfInfo.groupId = Loop::instance().groupId();
    selfInfo.hostPort = self.serverPort();

    dbg(0, NetworkGeneral)
        << "Server information:\n"
        << "\n           Address :" << self.serverAddress()
        << "\n              Port :" << self.serverPort()
        << "\n         Listening :" << self.isListening()
        << "\n            HostID :" << Debug::pretty(selfInfo.hostId)
        << "\n                ID :" << Debug::pretty(selfInfo.groupId)
        << "\n"
        ;

    // Sending UDP broadcast
    QByteArray datagram = QByteArray::number(self.serverPort()) + ":VoyService";

    dbg(0, NetworkLow) << "Broadcasting our presence:" << datagram;

    if (udpListener.writeDatagram(datagram.data(), datagram.size(),
                                  QHostAddress::Broadcast, VOY_DEFAULT_PORT) == -1) {
        dbg(0, Warning) << "Failed to broadcast";
    }

    if (self.serverPort() != VOY_DEFAULT_PORT) {
        // If we are not listening on the default port,
        // there is most likely our main server instance
        // that we should connect to
        connectToSource(PeerInfo(QHostAddress::LocalHost, VOY_DEFAULT_PORT));
    }

    self | on_new_client_available {
        QTcpSocket *socket = nullptr;
        while ((socket = self.nextPendingConnection())) {
            auto channel = new TcpChannel(socket, *q);

            dbg(channel, NetworkGeneral)
                << "Registering a new network sink:" << socket;

            connectToChannel(channel);
        }
    };
}


ConnectionManager::ConnectionManager(QObject *parent)
    : QObject(parent)
    , d(new Private(this))
{
    QTimer::singleShot(100, this, [this] {
            d->initializeServer();
            d->initializeDNSSD();
        });
}


// KDNSSD Part


void ConnectionManager::Private::initializeDNSSD()
{
    // Registering ourselves on DNSSD

    return;

    selfAnn.setServiceName(VOY_APPLICATION_NAME + "_" + Loop::instance().groupId().toString());
    selfAnn.setType("_" + VOY_APPLICATION_SLUG + "._tcp");
    selfAnn.setPort(self.serverPort());

    selfAnn | on_dnssd_service_published(published) {

        dbg(0, NetworkLow) << "Registered on KDNSSD:" << published;

        // Browsing our friends on DNSSD

        dnssdBrowser | on_dnssd_service_appeared(service) {

            service | on_dnssd_service_resolved(resolved) {
                if (!resolved) {
                    return;
                }

                connectToSource(
                        PeerInfo(
                            KDNSSD::ServiceBrowser::resolveHostName(service->hostName()),
                            service->port(),
                            QUuid(service->serviceName().split('_')[1])
                        )
                    );
            };

            service->resolveAsync();
        };

        dnssdBrowser.startBrowse();
    };

    selfAnn.publishAsync();
}


// Other


void ConnectionManager::Private::printServiceStatus()
{
    if (connectingChannels.size() > 0) {
        dbg(0, Warning)
            << "Stats:"
            << validChannels.size() << "channels, connecting to"
            << connectingChannels.size() << "valid"
            ;

    } else {
        dbg(0, NetworkGeneral)
            << "Stats:"
            << validChannels.size() << "channels"
            ;
    }

    dbg(0, NetworkLow) << "Known sources:";
    for (const auto &peer: peerInfos) {
        dbg(0, NetworkLow) << "\t" << peer;
    }
}


PeerInfoList ConnectionManager::connectedPeers() const
{
    return d->peerInfos;
}


PeerInfo ConnectionManager::selfInfo() const
{
    return d->selfInfo;
}

} // namespace Voy

