/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "PeerInfo.h"

#include <QDataStream>

#include <voy/utils/debug.h>

namespace Voy {

PeerInfo::PeerInfo(QHostAddress host, quint16 port, QUuid hostId, QUuid groupId)
    : hostAddress(host)
    , hostPort(port)
    , hostId(hostId)
    , groupId(groupId)
{
}


PeerInfo::PeerInfo(QUuid hostId, QUuid groupId)
    : hostId(hostId)
    , groupId(groupId)
{
}


bool PeerInfo::operator== (const PeerInfo &other) const
{
    return
        (groupId.isNull() || other.groupId.isNull())
            ? (hostAddress == other.hostAddress && hostPort == other.hostPort)
            : (groupId == other.groupId); // no need to check for hostId
}


bool PeerInfo::operator!= (const PeerInfo &other) const
{
    return !operator==(other);
}


QDebug operator<< (QDebug out, const Voy::PeerInfo &peerInfo)
{
    return out << (peerInfo.hostAddress.toString() + ":"
                   + QString::number(peerInfo.hostPort))
               << Debug::pretty(peerInfo.groupId)
               ;
}


QDataStream &operator<< (QDataStream &output, const Voy::PeerInfo &info)
{
    return output
        << info.hostAddress
        << info.hostPort
        << info.hostId
        << info.groupId
        ;
}


QDataStream &operator>> (QDataStream &input, Voy::PeerInfo &info)
{
    return input
        >> info.hostAddress
        >> info.hostPort
        >> info.hostId
        >> info.groupId
        ;
}


} // namespace Voy


uint qHash(const Voy::PeerInfo &peerInfo)
{
    return qHash(peerInfo.hostAddress) ^ qHash(peerInfo.hostPort);
}

