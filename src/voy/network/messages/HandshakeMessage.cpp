/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "HandshakeMessage.h"

#include <QDataStream>
#include <QDebug>

#include <voy/utils/debug.h>

namespace Voy {

REGISTER_MESSAGE_TYPE(HandshakeMessage)

HandshakeMessage::HandshakeMessage(QUuid hostId, const QUuid &groupId,
                                   quint16 port)
    : Message(CREATE_MESSAGE_HEADER)
    , hostId(hostId)
    , groupId(groupId)
    , port(port)
{
}


void HandshakeMessage::serialize(QDataStream &out) const
{
    out
        << hostId
        << groupId
        << port;
}


void HandshakeMessage::deserialize(QDataStream &in)
{
    in
        >> hostId
        >> groupId
        >> port;
}


QDebug &HandshakeMessage::debug(QDebug &out) const
{
    return Message::debug(out)
        << "HandshakeMessage("
        << Debug::pretty({ hostId, groupId }) << port
        << ")"
        ;
}

} // namespace Voy

