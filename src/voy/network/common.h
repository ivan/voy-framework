/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOY_NETWORK_COMMON_H
#define VOY_NETWORK_COMMON_H

#include <QIODevice>
#include <QDataStream>

#define VOY_APPLICATION_NAME QString("KRunner")
#define VOY_APPLICATION_SLUG VOY_APPLICATION_NAME.toLower()

#define VOY_DEFAULT_PORT 22151

#define VOY_DATA_STREAM_VERSION QDataStream::Qt_5_5

#define VOY_DECL_DATA_STREAM(Name, Buffer)                                     \
    QDataStream Name(&Buffer, QIODevice::ReadWrite);                           \
    Name.setVersion(VOY_DATA_STREAM_VERSION)

namespace Voy {
typedef qint64 message_size_t;
} // namespace Voy

#endif // include guard end

