/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "TcpChannel.h"

#include <QTcpSocket>
#include <QHostAddress>
#include <QTimer>

#include "../utils/debug.h"
#include "../Message.h"
#include "../MessageMatcher.h"
#include "../Loop.h"
#include "../Protocol.h"

#include "ConnectionManager.h"
#include "PeerInfo.h"

#include "messages/HandshakeMessage.h"
#include "messages/ServerListMessage.h"

#include "common.h"

#define VOY_TCP_CHANNEL_CONNECTION_SYNTAX
#include "../utils/inlineconnector.h"

namespace Voy {

class TcpChannel::Private: public QObject {
public:
    QTcpSocket *socket;
    TcpChannel *const q;
    const ConnectionManager &manager;
    PeerInfo peerInfo;
    bool connected;


    enum ChannelInitiator {
        Self,
        Remote
    } initiator;


    message_size_t announcedSize = 0;


    Private(QTcpSocket *socket, TcpChannel *parent,
            const ConnectionManager &manager)
        : socket(socket)
        , q(parent)
        , manager(manager)
        , connected(false)
    {
    }


    void init()
    {
        connect(socket, &QTcpSocket::readyRead,    q, [&] { onReadyRead(); });
        connect(socket, &QTcpSocket::connected,    q, [&] { onConnected(); });
        connect(socket, &QTcpSocket::disconnected, q, [&] { onDisconnected(); });

        connect(socket,
                static_cast<void (QTcpSocket::*)(QAbstractSocket::SocketError)>(
                    &QTcpSocket::error),
                q, [&](QAbstractSocket::SocketError error) {
                    onError(error);
                });
    }


    void onReadyRead()
    {
        if (announcedSize == 0) {
            // Waiting for the message size
            if (socket->bytesAvailable()
                    >= (message_size_t)sizeof(message_size_t)) {
                auto data = socket->read(sizeof(message_size_t));
                VOY_DECL_DATA_STREAM(input, data);
                input >> announcedSize;
            }
        }

        if (announcedSize != 0) {
            // We have the message size, can we read the message now?
            if (socket->bytesAvailable() >= announcedSize) {

                auto data = socket->read(announcedSize);
                VOY_DECL_DATA_STREAM(input, data);

                MessageHeader header;
                input >> header;

                auto message = MessageTypes::create(header.messageCategory,
                                                    header.messageType);

                // If we are not connected yet, we are accepting
                // messages from "unknown" agents - that is, the
                // group will send us messages, not a specific agent
                // TODO: Maybe the group handler (TcpChannel) should
                // itself be an agent...
                Q_ASSERT_X(!connected || !header.from.agentId.isNull(),
                           "TcpChannel::onReadyRead",
                           "We are not accepting messages from non-agents");
                Q_ASSERT_X(!header.from.groupId.isNull(),
                           "TcpChannel::onReadyRead",
                           "Message needs to have the group defined");
                Q_ASSERT_X(!header.from.hostId.isNull(),
                           "TcpChannel::onReadyRead",
                           "Message needs to have the host defined");

                if (message) {
                    message->setHeader(header);
                    message->deserialize(input);

                    dbg(q, NetworkLow)
                        << "Got a message from:"
                        << header.from
                        ;

                    emit q->messageReceived(message);

                } else {
                    dbg(q, Warning) << "Got an unknown message:" << header;
                }

                announcedSize = 0;

                QTimer::singleShot(0, q, [&] { onReadyRead(); });
            }
        }
    }


    // TODO: Replace the state machine with causeway-like syntax


    enum {
        SendInitialHandshake,
        WaitForInitialHandshake,
        AcceptInitialHandshake,
        AcceptResponseHandshake,
        WaitForServerListMessage,
        AcceptServerListMessage,
        End
    } nextConnectionStep;
    QMetaObject::Connection connection;

    void performNextConnectionStep(const MessagePtr &message)
    {
        // dbg(q, NetworkSink) << "Got the message" << message;
        auto selfHostId  = Loop::instance().hostId();
        auto selfGroupId = Loop::instance().groupId();

        switch (nextConnectionStep) {
            case SendInitialHandshake:
                dbg(q, NetworkSend) << "Sending the initial handshake:"
                                    << Debug::pretty({ selfHostId, selfGroupId });

                Voy::Protocol::sendVia<HandshakeMessage>(
                    q, selfHostId, selfGroupId, manager.selfInfo().hostPort);

                nextConnectionStep = //AcceptInitialHandshake - server
                                     AcceptResponseHandshake;
                break;


            case WaitForInitialHandshake:
                // do nothing :)
                dbg(q, NetworkReceive) << "Waiting for the initial handshake...";
                nextConnectionStep = AcceptInitialHandshake;
                break;


            case AcceptInitialHandshake:
            {
                message
                    | V_MATCH(HandshakeMessage) {
                        dbg(q, NetworkReceive) << "Got the initial handshake:"
                                               << Debug::pretty({ _.hostId, _.groupId })
                                               << _.port;

                        peerInfo.hostId   = _.hostId;
                        peerInfo.groupId  = _.groupId;
                        peerInfo.hostPort = _.port;

                        dbg(q, NetworkSend) << "Sending the response handshake:"
                                            << DEBUG_COLOR_VALUE
                                            << Debug::pretty({ selfHostId, selfGroupId });

                        Voy::Protocol::sendVia<HandshakeMessage>(
                            q, selfHostId, selfGroupId,
                            manager.selfInfo().hostPort);

                        dbg(q, NetworkSend) << "Sending the server list:"
                                            << DEBUG_COLOR_VALUE
                                            << manager.connectedPeers();

                        Voy::Protocol::sendVia<ServerListMessage>(
                            q, manager.connectedPeers());

                        nextConnectionStep = // AcceptResponseHandshake - client
                                             // AcceptServerList - client
                                             AcceptServerListMessage;
                    }
                    | V_OTHERWISE {
                        dbg(q, Warning) << "Unexpected message: " << _;
                    }
                ;

                break;
            }


            case AcceptResponseHandshake:
            {
                auto handshake = message->as<HandshakeMessage>();
                dbg(q, NetworkReceive) << "Got the response handshake:"
                                       << DEBUG_COLOR_VALUE
                                       << Debug::pretty({ handshake.hostId, handshake.groupId })
                                       << handshake.port;

                peerInfo.hostId   = handshake.hostId;
                peerInfo.groupId  = handshake.groupId;
                peerInfo.hostPort = handshake.port;

                dbg(q, NetworkSend) << "Sending the server list:"
                                    << DEBUG_COLOR_VALUE
                                    << manager.connectedPeers();

                Voy::Protocol::sendVia<ServerListMessage>(
                    q, manager.connectedPeers());

                nextConnectionStep = // AcceptServerListMessage - server
                                     AcceptServerListMessage;
                break;
            }


            case AcceptServerListMessage:
            {
                auto serverList = message->as<ServerListMessage>();
                dbg(q, NetworkReceive) << "Got the server list:"
                                       << DEBUG_COLOR_VALUE
                                       << serverList.servers;
                nextConnectionStep = End;

                // dbg(q, Success) << "Connected!";
                emit q->connected();
                QObject::disconnect(connection);

                emit q->serverListReceived(serverList.servers);

                connected = true;

                break;
            }


            default:
                dbg(q, Error) << "We are not in Kansas anymore...";
                emit q->error(QAbstractSocket::UnknownSocketError);
                QObject::disconnect(connection);
        }
    }


    void onConnected()
    {
        // We are not sending the connect event just yet,
        // we need to verify the connection
        // emit q->connected();

        // Client starts the communication
        nextConnectionStep = (initiator == Self)
                                ? SendInitialHandshake
                                : WaitForInitialHandshake
                                ;

        connection = q | on_new_message(message) {
            performNextConnectionStep(message);
        };

        performNextConnectionStep(MessagePtr());

    }

    //

    void onDisconnected()
    {
        emit q->disconnected();
    }


    void onError(QAbstractSocket::SocketError error)
    {
        emit q->error(error);
    }


    static QTcpSocket *connectToHost(const PeerInfo &peer)
    {
        auto socket = new QTcpSocket();
        socket->connectToHost(peer.hostAddress, peer.hostPort);
        return socket;
    }

};


TcpChannel::TcpChannel(QTcpSocket *socket, ConnectionManager &manager)
    : QObject(socket)
    , d(new Private(socket, this, manager))
{
    dbg(this, NetworkLow) << "TcpChannel::constructor - sink";

    d->peerInfo.hostAddress = socket->peerAddress();
    d->peerInfo.hostPort = socket->peerPort();

    d->initiator = Private::Remote;

    d->init();

    // We are already connected
    d->onConnected();
}


TcpChannel::TcpChannel(const PeerInfo &peer,
                       ConnectionManager &manager)
    : QObject()
    , d(new Private(Private::connectToHost(peer), this, manager))
{
    dbg(this, NetworkLow) << "TcpChannel::constructor - source";

    d->peerInfo = peer;

    // setParent(d->socket); // TODO: ???

    d->initiator = Private::Self;

    d->init();
}


TcpChannel::~TcpChannel()
{
    d->socket->deleteLater();
    delete d;
}


QTcpSocket *TcpChannel::socket() const
{
    return d->socket;
}


void TcpChannel::sendMessage(MessagePtr message)
{
    dbg(this, NetworkLow) << "Sending message:" << message;

    QByteArray messageData;
    VOY_DECL_DATA_STREAM(messageDataStream, messageData);

    // Writing the header and the message
    messageDataStream << message->headerRef();

    message->serialize(messageDataStream);

    QByteArray messageSizeData;
    VOY_DECL_DATA_STREAM(messageSizeDataStream, messageSizeData);

    message_size_t size = messageData.size();
    messageSizeDataStream << size;

    d->socket->write(messageSizeData);
    d->socket->write(messageData);
}


PeerInfo TcpChannel::peerInfo() const
{
    return d->peerInfo;
}


Address TcpChannel::peerAddress() const
{
    return group(d->peerInfo.hostId, d->peerInfo.groupId);
}

} // namespace Voy

