/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VOY_NETWORK_CONNECTION_MANAGER_P_H
#define VOY_NETWORK_CONNECTION_MANAGER_P_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QUdpSocket>

#include <DNSSD/ServiceBrowser>
#include <DNSSD/PublicService>

#include <QHash>
#include <QUuid>
#include <QTimer>

#include "PeerInfo.h"

namespace Voy {

class TcpChannel;

class ConnectionManager::Private: public QObject {
    Q_OBJECT

public:
    Private(ConnectionManager *parent);

    void connectToChannel(TcpChannel *channel);
    void removeChannel(TcpChannel *channel);

    void connectToSource(const PeerInfo &peerInfo);

    void printServiceStatus();

    void initializeServer();
    void initializeDNSSD();

    bool isLocalHost(const PeerInfo &peer) const;

public:
    ConnectionManager *const q;
    KDNSSD::ServiceBrowser dnssdBrowser;

    QSet<TcpChannel*> connectingChannels;
    QHash<QUuid, TcpChannel*> validChannels;
    PeerInfoList peerInfos;

    QUdpSocket udpListener;

    QTcpServer self;
    PeerInfo selfInfo; // This might not have any value inside
    KDNSSD::PublicService selfAnn;

    QTimer printServiceStatusTimer;
};

} // namespace Voy

#endif // include guard end
