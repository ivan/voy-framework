/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MESSAGEMATCHER_H
#define MESSAGEMATCHER_H

#include "Message.h"

#include "utils/debug.h"

namespace Voy {
namespace detail {

template <typename MessageType, typename Function>
struct StaticMatcher {
    StaticMatcher(const char *location, Function function)
        : location(location)
        , function(function)
    {
    }

    const char *location;
    Function function;
};

template <typename MessageType>
struct StaticMatcherHelper {
    StaticMatcherHelper(const char *location)
        : location(location)
    {
    }

    template <typename Function>
    StaticMatcher<MessageType, Function> operator* (Function &&function) {
        return StaticMatcher<MessageType, Function>(
                location,
                std::forward<Function>(function)
            );
    }

    const char *location;
};

struct MessageHolder {
    explicit MessageHolder(MessagePtr message)
        : message(message)
    {
    }

    MessageHolder(MessageHolder &&original)
        : location(original.location)
        , message(original.message)
    {
        original.location = nullptr;
        original.message.reset();
    }

    ~MessageHolder()
    {
        if (message) {
            dbg(0, Error)
                << "Message has not been handled:"
                << message
                << "Location:"
                << location;
            qFatal("Non-exausted matches");
        }
    }

    const char *location;
    MessagePtr message;
};

template <typename Function>
void operator | (MessageHolder &&holder,
                 const StaticMatcher<void, Function> &matcher)
{
    if (holder.message) {
        matcher.function(holder.message);
        holder.message.reset();
    }
}

template <typename MessageType, typename Function>
MessageHolder operator | (MessageHolder &&holder,
                          const StaticMatcher<MessageType, Function> &matcher)
{
    if (holder.message && holder.message->is<MessageType>()) {
        matcher.function(holder.message->as<MessageType>());
        holder.message.reset();
    } else {
        holder.location = matcher.location;
    }

    return std::move(holder);
}

} // namespace detail


template <typename MessageType>
detail::StaticMatcherHelper<MessageType> match(const char* location)
{
    return detail::StaticMatcherHelper<MessageType>(location);
}

template <typename MessageType, typename Function>
detail::MessageHolder operator | (const MessagePtr &message,
                                  const detail::StaticMatcher<MessageType, Function> &matcher)
{
    return detail::MessageHolder(message) | matcher;
}

} // namespace Voy

#define V_STRINGIFY(X) #X
#define V_TOSTRING(X) V_STRINGIFY(X)
#define V_CODE_LOCATION __FILE__ ":" V_TOSTRING(__LINE__)

#define V_MATCH(Type) Voy::match<Type>(V_CODE_LOCATION) * [&] (const Type &_)
#define V_OTHERWISE                                                            \
    Voy::match<void>(V_CODE_LOCATION) *[&](const Voy::MessagePtr &_)
#define V_OTHERWISE_IGNORE                                                     \
    V_OTHERWISE                                                                \
    {                                                                          \
        qDebug() << "Warning: Unmatched message:" << _ << V_CODE_LOCATION;     \
    }

#endif // include guard end

