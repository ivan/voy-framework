
set (
   Voy_SRCS
   # main.cpp

   voy/Agent.cpp
   voy/Address.cpp
   voy/Loop.cpp
   voy/Message.cpp
   voy/MessageHeader.cpp
   voy/MessageTypes.cpp

   voy/network/ConnectionManager.cpp
   voy/network/PeerInfo.cpp
   voy/network/TcpChannel.cpp
   voy/network/messages/HandshakeMessage.cpp
   voy/network/messages/ServerListMessage.cpp

   voy/utils/debug.cpp
   )

add_library (
   VoyExperimental
   ${Voy_SRCS}
   )

generate_export_header (VoyExperimental BASE_NAME Voy)

# add_executable (
#    voy
#    ${Voy_SRCS}
#    )

target_link_libraries (
   VoyExperimental

   Qt5::Sql
   Qt5::Quick
   Qt5::Qml
   Qt5::Sql
   Qt5::Widgets

   KF5::DNSSD
   KF5::ConfigCore
   )

