/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TEST_CONTROLLER_AGENT_H
#define TEST_CONTROLLER_AGENT_H

#include <voy/Agent.h>

#include <QTimer>

class TestControllerAgent: public Voy::Agent {
public:
    TestControllerAgent(const QByteArray &team);

    void message(Voy::MessagePtr message) override;

    void sendNextMessage();

    void check();

private:
    enum Steps {
        SendToEveryone = 0,
        SendToHost = 1,
        SendToGroup = 2,
        SendToTeam = 3,
        SendToTeamHost = 4,
        SendToTeamGroup = 5,
        SendQuit = 6
    };

    Steps step;

    QTimer stepper;

    quint8 allAgentsCount = 0;
    quint8 localAgentsCount = 0;
    quint8 groupAgentsCount = 0;

    quint8 allTeamAgentsCount = 0;
    quint8 localTeamAgentsCount = 0;
    quint8 groupTeamAgentsCount = 0;

    quint8 numberOfReplies = 0;

    quint8 numberOfTests = 0;
    quint8 numberOfFailures = 0;
};

#endif // include guard end

