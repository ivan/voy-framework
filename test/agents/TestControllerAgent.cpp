/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "TestControllerAgent.h"

#include <voy/Loop.h>

#include <QDebug>
#include <QCoreApplication>

#include <voy/MessageMatcher.h>
#include <voy/utils/debug.h>
using Voy::Debug::pretty;

#include "messages/TestBroadcastMessage.h"
#include "messages/TestBroadcastReplyMessage.h"
#include "messages/TestQuitMessage.h"

TestControllerAgent::TestControllerAgent(const QByteArray &team)
    : Agent(team)
    , step(SendToEveryone)
{
    QTimer::singleShot(10000, [=] {
        stepper.setInterval(2000);
        stepper.setSingleShot(false);
        QObject::connect(&stepper, &QTimer::timeout,
                &stepper, [&] {
                    sendNextMessage();
                }
            );

        dbg(this, Voy::Other) << pretty(id()) << "Starting with the message flood";
        stepper.start();
    });
}

void TestControllerAgent::sendNextMessage()
{
    using Voy::world;
    using Voy::localhost;
    using Voy::group;

    // dbg(this, Voy::Other) << pretty(agentId()) << "Sending the message...";

    const auto data = QString::number(step) + " <- the msg";

    check();

    qDebug() << "\n\n";

    switch (step) {
        case SendToEveryone:
            // Sending a message to everyone
            dbg(this, Voy::Other)
                << "SendToEveryone: Sending a message to everyone";
            broadcast<TestBroadcastMessage>(data, step);
            // Same as: send<TestBroadcastMessage>(world());
            break;


        case SendToHost:
            // Sending a message to everyone on this host
            dbg(this, Voy::Other)
                << "SendToHost: Sending a message to everyone on this host";
            send<TestBroadcastMessage>(localhost(), data, step);
            break;


        case SendToGroup:
            // Sending a message to everyone in this group
            dbg(this, Voy::Other)
                << "SendToGroup: Sending a message to everyone in this group";
            send<TestBroadcastMessage>(group(), data, step);
            break;


        case SendToTeam:
            // Sending a message to all members of the Team
            dbg(this, Voy::Other)
                << "SendToTeam: Sending a message to all members of the Team";
            send<TestBroadcastMessage>(world("Team"), data, step);
            break;


        case SendToTeamHost:
            // Sending a message to all local members of the Team
            dbg(this, Voy::Other)
                << "SendToTeamHost: Sending a message to all local members of the Team";
            send<TestBroadcastMessage>(localhost("Team"), data, step);
            break;


        case SendToTeamGroup:
            // Sending a message to all group members of the Team
            dbg(this, Voy::Other)
                << "SendToTeamGroup: Sending a message to all group members of the Team";
            send<TestBroadcastMessage>(group("Team"), data, step);
            break;


        case SendQuit:
            send<TestQuitMessage>(localhost("Watchmen"));
            break;


        default:
            // Printing summaries

            if (numberOfFailures) {
                dbg(0, Voy::Error)
                    << "--- [ FAILURE ]"
                    << numberOfFailures
                    << "of"
                    << numberOfTests
                    << "tests failed ---";

            } else {
                dbg(0, Voy::Success)
                    << "--- [ SUCCESS ] All tests have passed ---";

            }

            dbg(this, Voy::Other) << "Quitting...";
            QCoreApplication::quit();

    }

    step = (Steps)(step + 1);
}

void TestControllerAgent::check()
{
    if (step == SendToEveryone) {
        // We are now sending the first broadcast,
        // nothing to check for

    } else if (step == SendToHost) {
        // We have now sent the broadcast to local
        // agents, this means we should have all the
        // agents that wish to reply collected

        dbg(this, Voy::Other)
            << "\n        All agents:          " << allAgentsCount
            << "\n        Local agents:        " << localAgentsCount
            << "\n        Group agents:        " << groupAgentsCount
            << "\n        All agents:   (team) " << allTeamAgentsCount
            << "\n        Local agents: (team) " << localTeamAgentsCount
            << "\n        Group agents: (team) " << groupTeamAgentsCount
            ;

        numberOfTests++;

        if (allAgentsCount < 4
            || groupAgentsCount == allAgentsCount
            || allTeamAgentsCount == allAgentsCount
            || groupTeamAgentsCount == allTeamAgentsCount
                ) {
            dbg(this, Voy::Error)
                << "Wrong number of replies";
            numberOfFailures++;
        }

    } else {
        auto prevStep = step - 1;

        auto expectedNumberOfReplies = (
                prevStep == SendToHost      ? localAgentsCount :
                prevStep == SendToGroup     ? groupAgentsCount :
                prevStep == SendToTeam      ? allTeamAgentsCount :
                prevStep == SendToTeamHost  ? localTeamAgentsCount :
                prevStep == SendToTeamGroup ? groupTeamAgentsCount :
                0
            );

        // auto stepName = (
        //         prevStep == SendToHost      ? "SendToHost"      :
        //         prevStep == SendToGroup     ? "SendToGroup"     :
        //         prevStep == SendToTeam      ? "SendToTeam"      :
        //         prevStep == SendToTeamHost  ? "SendToTeamHost"  :
        //         prevStep == SendToTeamGroup ? "SendToTeamGroup" :
        //         "end"
        //     );
        //
        // qDebug()
        //     << "###"
        //     << expectedNumberOfReplies
        //     << "("
        //     << allAgentsCount
        //     << localAgentsCount
        //     << groupAgentsCount
        //     << allTeamAgentsCount
        //     << localTeamAgentsCount
        //     << groupTeamAgentsCount
        //     << ")"
        //     << stepName;

        numberOfTests++;

        if (expectedNumberOfReplies == numberOfReplies) {
            dbg(this, Voy::Success)
                << "All is well"
                << "( expected" << expectedNumberOfReplies
                << " and got" << numberOfReplies << "replies )"
                ;

        } else {
            numberOfFailures++;
            dbg(this, Voy::Error)
                << "Not enough replies"
                << "( expected" << expectedNumberOfReplies
                << " but received" << numberOfReplies << ")"
                ;
        }
    }

    numberOfReplies = 0;
}

void TestControllerAgent::message(Voy::MessagePtr message)
{
    message
        | V_MATCH(TestBroadcastReplyMessage) {

            const auto &from = _.from();

            dbg(this, Voy::Highlight)
                << "Controller received a message: "
                << DEBUG_COLOR_CYAN
                << _.name
                << _.originalMessageId
                << _.teams
                << COLOR_RESET
                << DEBUG_COLOR_SLIM_CYAN
                << " from:"
                << from
                ;


            numberOfReplies++;

            if (_.originalMessageId == 0) {

                allAgentsCount++;
                if (_.teams.contains("Team")) { allTeamAgentsCount++; }

                if (from.hostId == Voy::Loop::instance().hostId()) {
                    localAgentsCount++;
                    if (_.teams.contains("Team")) { localTeamAgentsCount++; }
                }

                if (from.groupId == Voy::Loop::instance().groupId()) {
                    groupAgentsCount++;
                    if (_.teams.contains("Team")) { groupTeamAgentsCount++; }
                }

            }
        }
    ;
}

