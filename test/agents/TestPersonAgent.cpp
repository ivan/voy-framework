/*
 *   Copyright (C) 2016 Ivan Čukić <ivan.cukic(at)kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) version 3, or any
 *   later version accepted by the membership of KDE e.V. (or its
 *   successor approved by the membership of KDE e.V.), which shall
 *   act as a proxy defined in Section 6 of version 3 of the license.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

#include "TestPersonAgent.h"

#include <voy/Loop.h>

#include <QDebug>
#include <QCoreApplication>

#include <voy/Message.h>
#include <voy/MessageMatcher.h>

#include <voy/utils/debug.h>
using Voy::Debug::pretty;

#include "../messages/TestBroadcastMessage.h"
#include "../messages/TestBroadcastReplyMessage.h"

TestPersonAgent::TestPersonAgent(const QByteArray &team, const QByteArray &name)
    : Agent(team)
    , m_name(name)
{
}

void TestPersonAgent::message(Voy::MessagePtr message)
{
    message
        | V_MATCH(TestBroadcastMessage) {
            Q_UNUSED(_);

            dbg(this, Voy::Other)
                << pretty(id())
                << name()
                << " (Person) received a message"
                << _.data
                << ". Replying..."
                ;

            reply<TestBroadcastReplyMessage>(name(), _.messageId, teams());
        }
    ;
}

const QByteArray &TestPersonAgent::name() const
{
    return m_name;
}

