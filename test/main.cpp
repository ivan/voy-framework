#include <QtGui>
#include <QtQuick>
#include <QDebug>
#include <QHash>
#include <QString>

#include <QApplication>

#include <voy/Loop.h>

#include "agents/TestPersonAgent.h"
#include "agents/TestWatchmanAgent.h"
#include "agents/TestSilentAgent.h"
#include "agents/TestControllerAgent.h"

#include <memory>

struct TestAgentInfo {
    QByteArray name;
    QByteArray team;

    enum {
        Person,
        Watchman,
        Silent,
        Controller
    } type;

};

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    qDebug() << "Args:" << QCoreApplication::arguments();

    QString command = QCoreApplication::arguments()[0];

    Voy::Loop::instance();

    int instance = QCoreApplication::arguments().size() < 2
                        ? 1
                        : QCoreApplication::arguments()[1].toInt();

    QList<TestAgentInfo> agentInfos;

    QList<QProcess*> processes;

    auto startSelf = [&] (const QString &arg) {
        auto process = new QProcess(&app);
        process->setProcessChannelMode(QProcess::ForwardedChannels);
        processes << process;
        // process->setStandardOutputFile("voy-test-output" + arg, QIODevice::Append);
        process->start(command, {arg});
    };

    switch (instance) {
        case 1:
            if (QCoreApplication::arguments().size() == 1) {
                // startSelf("1");
                startSelf("2");
                startSelf("3");
            }

            agentInfos =  {
                { "",       "",         TestAgentInfo::Controller },
                { "Alice",  "Team",     TestAgentInfo::Person },
                { "Bob",    "Team",     TestAgentInfo::Person }
                // { "Walter", "Watchmen", TestAgentInfo::Watchman }
            };
            break;

        case 2:
            agentInfos = {
                { "Carol",  "Team",     TestAgentInfo::Person },
                { "Dan",    "Team",     TestAgentInfo::Person },
                { "Trent",  "",         TestAgentInfo::Person },
                { "Wes",    "Watchmen", TestAgentInfo::Watchman }
            };
            break;

        case 3:
            agentInfos = {
                { "Erin" ,  "",         TestAgentInfo::Person },
                { "Frank",  "",         TestAgentInfo::Person },
                { "Wendy" , "Watchmen", TestAgentInfo::Watchman }
            };
            break;

        default:
            return 1;
    }

    for (const auto &agentInfo: agentInfos) {
        Voy::Agent *agent;

        switch (agentInfo.type) {
            case TestAgentInfo::Controller:  agent = new TestControllerAgent (agentInfo.team);                 break;
            case TestAgentInfo::Person:      agent = new TestPersonAgent     (agentInfo.team, agentInfo.name); break;
            case TestAgentInfo::Watchman:    agent = new TestWatchmanAgent   (agentInfo.team, agentInfo.name); break;
            case TestAgentInfo::Silent:      agent = new TestSilentAgent     (agentInfo.team, agentInfo.name); break;
        }

        if (!agentInfo.team.isEmpty()) {
            agent->setTeam(agentInfo.team);
        }
    }

    return app.exec();
}

