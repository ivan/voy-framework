
set (
   VoyTest_SRCS

   main.cpp

   messages/TestBroadcastMessage.cpp
   messages/TestBroadcastReplyMessage.cpp
   messages/TestQuitMessage.cpp

   agents/TestControllerAgent.cpp
   agents/TestPersonAgent.cpp
   agents/TestSilentAgent.cpp
   agents/TestWatchmanAgent.cpp
   )

add_executable (
   voy-test
   ${VoyTest_SRCS}
   )

target_link_libraries (
   voy-test

   VoyExperimental

   Qt5::Sql
   Qt5::Quick
   Qt5::Qml
   Qt5::Sql
   Qt5::Widgets

   KF5::DNSSD
   KF5::ConfigCore
   )

